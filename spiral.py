#!/usr/bin/env python

import sys

# where the spiral printing starts on each iteration
NEW_ITERATION_START = 1

class Spiral(object):
    def __init__(self, file_name):
        self.matrix = self.get_matrix_from_file(file_name)
        self.results = []
        self.num_columns = -1
        self.num_rows = -1

    def get_matrix_from_file(self, file_name):
        matrix = []
        with open(file_name) as f:
            for line in f:
                if line.startswith('***'):
                    continue

                line = line.strip()
                matrix.append(line.split())

        return matrix

    def resize_matrix(self):
        # On every iteration, the spiral starts at
        # (NEW_ITERATION_START, NEW_ITERATION_START)
        # This function returns a new matrix starting at NEW_ITERATION_START and
        # chops off the outside layer of the matrix
        return [row[NEW_ITERATION_START:-NEW_ITERATION_START] for row in
                self.matrix[NEW_ITERATION_START:-NEW_ITERATION_START]]

    def get_top(self):
        for number in self.matrix[0]:
            self.results.append(number)

    def get_right(self):
        for row in self.matrix[1:]:
            self.results.append(row[self.num_columns - 1])

    def get_bottom(self):
        for i, column in enumerate(reversed(self.matrix[self.num_rows - 1][:-1])):
            self.results.append(column[:self.num_columns - i - 1])

    def get_left(self):
        for row in reversed(self.matrix[1:-1]):
            self.results.append(row[0])

    def spiral(self):
        self.num_rows = len(self.matrix)
        self.num_columns = len(self.matrix[0])

        while self.matrix:
            self.get_top()
            self.get_right()
            self.get_bottom()
            self.get_left()

            self.matrix = self.resize_matrix()

            self.num_rows = len(self.matrix)
            try:
                self.num_columns = len(self.matrix[0])
            except IndexError:
                # special case when we are left with a 1x1 matrix
                self.num_columns = 1

    def print_results(self):
        print '***\n%s\n***' % ' '.join(self.results)

if __name__ == '__main__':
    file_name = sys.argv[1]

    spiral = Spiral(file_name)
    spiral.spiral()
    spiral.print_results()

    # TODO:
    # error checking: check if matrix is empty
    # check if matrix format is correct (n x m)
