import unittest
import sys
sys.path.append('../')
from spiral import Spiral

class SpiralTests(unittest.TestCase):
    def test_read_from_file(self):
        spiral = Spiral('input1')
        self.assertEquals(spiral.matrix, [['1', '2'], ['3', '4']])

    def test_resize_matrix(self):
        spiral = Spiral('input3')
        spiral.resize_matrix()
        self.assertEquals(spiral.matrix,
            [['1', '2', '3', '4', '5', '6'],
             ['0', '4', '3', '4', '5', '7'],
             ['9', '3', '3', '4', '6', '8'],
             ['8', '2', '3', '4', '7', '9'],
             ['7', '1', '0', '9', '8', '0'],
             ['6', '5', '4', '3', '2', '1']])

    def test_single(self):
        spiral = Spiral('input4')
        spiral.spiral()
        self.assertEquals(spiral.results, ['1'])

    def test_spiral(self):
        spiral = Spiral('input3')
        spiral.spiral()
        self.assertEquals(spiral.results, ['1', '2', '3', '4', '5', '6', '7',
            '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
            '4', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '3',
            '4', '4', '3'])

class SideTests(unittest.TestCase):
    def setUp(self):
        self.spiral = Spiral('input2')
        self.spiral.num_rows = len(self.spiral.matrix)
        self.spiral.num_columns = len(self.spiral.matrix[0])

    def tearDown(self):
        self.results = []

    def test_get_top(self):
        self.spiral.get_top()
        self.assertEquals(self.spiral.results, ['1', '2', '3'])

    def test_get_right(self):
        self.spiral.get_right()
        self.assertEquals(self.spiral.results, ['6', '9'])

    def test_get_bottom(self):
        self.spiral.get_bottom()
        self.assertEquals(self.spiral.results, ['8', '7'])

    def test_get_left(self):
        self.spiral.get_left()
        self.assertEquals(self.spiral.results, ['4'])

if __name__ == '__main__':
    unittest.main()
